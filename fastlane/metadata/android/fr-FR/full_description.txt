
Paseo utilise le capteur de pas intégré d'un appareil Android pour conserver un historique des pas effectués (marcher, courir, sauter ou toute autre activité que l'appareil interprète comme des pas).

Paseo affiche l'historique des étapes dans plusieurs formats, y compris des tableaux et des graphiques pour les étapes par heure, jour, semaine, mois et année.

Paseo ne transmet ni ne partage aucune donnée. Paseo stocke les données uniquement sur l'appareil sur lequel il est installé. 

